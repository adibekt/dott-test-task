//
//  VehicleViewController.swift
//  iOS-assignment
//
//  Created by tair on 11.04.22.
//

import UIKit

class VehicleViewController: UIViewController, Nibbed {

    @IBOutlet weak var imgQRCode: DottImageView!
    @IBOutlet weak var lblCode: TitleLabel!
    
    var item: Vehicle?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initViews()
    }
    
    func initViews() {
        guard let item = item else {
            return
        }
        lblCode.text = item.identificationCode
        imgQRCode.image = generateQRCode(from: item.qrURL.absoluteString)
    }

    @IBAction func onDismissPressed(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
   
    private func generateQRCode(from string: String) -> UIImage? {
        let data = string.data(using: String.Encoding.ascii)
        if let filter = CIFilter(name: "CIQRCodeGenerator") {
            filter.setValue(data, forKey: "inputMessage")
            let transform = CGAffineTransform(scaleX: 3, y: 3)
            if let output = filter.outputImage?.transformed(by: transform) {
                return UIImage(ciImage: output)
            }
        }
        return nil
    }
}
