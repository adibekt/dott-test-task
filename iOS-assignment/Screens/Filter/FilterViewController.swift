//
//  FilterViewController.swift
//  iOS-assignment
//
//  Created by tair on 11.04.22.
//

import UIKit
protocol Filterred {
    func picked(colors: [Color])
}
class FiltersViewController: UIViewController, Nibbed {

    var delegate: Filterred?
    var selectedColors: [Color] = Color.allCases
    
    @IBOutlet weak var swRedGreen: UISwitch!
    @IBOutlet weak var swBlueRed: UISwitch!
    @IBOutlet weak var swPinkYellow: UISwitch!
    @IBOutlet weak var swYellowBlue: UISwitch!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        initViews()
    }
    
    // TODO: Create UI another way and refactor this
    func initViews() {
        swRedGreen.isOn = false
        swBlueRed.isOn = false
        swPinkYellow.isOn = false
        swYellowBlue.isOn = false
        swRedGreen.tag = 0
        swBlueRed.tag = 1
        swPinkYellow.tag = 2
        swYellowBlue.tag = 3
        
        for color in selectedColors {
            switch color {
            case .blueYellow:
                self.swYellowBlue.isOn = true
            case .redGreen:
                self.swRedGreen.isOn = true
            case .blueRed:
                self.swBlueRed.isOn = true
            case .pinkYellow:
                self.swPinkYellow.isOn = true
            }
        }
        
        swRedGreen.addTarget(self, action: #selector(self.valueChanged(_:)), for: .valueChanged)
        swBlueRed.addTarget(self, action: #selector(self.valueChanged(_:)), for: .valueChanged)
        swPinkYellow.addTarget(self, action: #selector(self.valueChanged(_:)), for: .valueChanged)
        swYellowBlue.addTarget(self, action: #selector(self.valueChanged(_:)), for: .valueChanged)
    }
    
    @IBAction func dismissPressed(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func applyPressed(_ sender: Any) {
        delegate?.picked(colors: selectedColors)
        self.dismiss(animated: true, completion: nil)
    }
   
    @objc func valueChanged(_ sender: UISwitch) {
        var color: Color!
        switch sender.tag {
        case 0:
            color = .redGreen
        case 1:
            color = .blueRed
        case 2:
            color = .pinkYellow
        case 3:
            color = .blueYellow
        default:
            print("color notFound")
        }
        if sender.isOn {
            if !(selectedColors.contains(color)) {
                selectedColors.append(color)
            }
        } else {
            var newArray: [Color] = []
            for icolor in selectedColors {
                if icolor != color {
                    newArray.append(icolor)
                }
            }
            selectedColors = newArray
        }
    }
}
