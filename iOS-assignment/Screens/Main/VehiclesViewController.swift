//
//  VehiclesViewController.swift
//  iOS-assignment
//
//  Created by tair on 11.04.22.
//

import UIKit
import RxSwift
import RxRelay
import RxCocoa


class VehiclesViewController: UIViewController, Nibbed, VehilclesViewProtocol {

    var presenter: VehilclesPresenterProtocol?
    @IBOutlet weak var tableView: DottTableView!
    @IBOutlet weak var btnFilter: FilterButton!
    let bag = DisposeBag()
    let refreshControl = UIRefreshControl()
    var errorView: ErrorView?
    var vehicles: [Vehicle] = []
    
    // MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpTableView()
        bindings()
        presenter?.getData()
    }
    
    private func setUpTableView() {
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UINib(nibName: VehicleCell.nibName, bundle: nil), forCellReuseIdentifier: VehicleCell.reuseIdentifier)
        refreshControl.addTarget(self, action: #selector(self.refresh), for: .valueChanged)
        tableView.addSubview(refreshControl)
    }
    
    @objc func refresh() {
        presenter?.getData()
    }
    
    // MARK: - Bindings
    func bindings() {
        presenter?.vehiclesList.observeOn(MainScheduler.instance)
            .subscribe { [weak self] data in
                guard let self = self else { return }
                self.vehicles = data.element ?? []
                self.refreshControl.endRefreshing()
                self.tableView.reloadData()
            }
            .disposed(by: bag)
        
        presenter?.isBusy.observeOn(MainScheduler.instance)
            .subscribe({ [weak self] isBusy in
                guard let self = self, let isLoading = isBusy.element else { return }
                if isLoading {
                    self.refreshControl.beginRefreshing()
                } else {
                    self.refreshControl.endRefreshing()
                }
            })
            .disposed(by: bag)
        
        presenter?.errorText.observeOn(MainScheduler.instance)
            .subscribe({ [weak self] errorText in
                guard let self = self, let text = errorText.element else { return }
                self.show(message: text)
                self.showErrorView()
            })
            .disposed(by: bag)
    }
    
    @IBAction func onFiltersPressed(_ sender: Any) {
        if !btnFilter.isFilterActive {
            presenter?.openFilter()
        } else {
            btnFilter.isFilterActive = false
            presenter?.clearFilters()
        }
    }
    
    private func showErrorView() {
        errorView = ErrorView(frame: CGRect(x: tableView.frame.origin.x + 15, y: tableView.frame.origin.y, width: tableView.frame.width - 30, height: 200))
        tableView.addSubview(errorView!)
        errorView?.buttonPressed = { [weak self] in
            self?.errorView?.removeFromSuperview()
            self?.refresh()
        }
    }
    
}

// MARK: - Filter Delegate
extension VehiclesViewController: Filterred {
    func picked(colors: [Color]) {
        if let filteredData = presenter?.filter(by: colors) {
            btnFilter.isFilterActive = true
            vehicles = filteredData
            tableView.reloadData()
        }
    }
}

// MARK: - Table View
extension VehiclesViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return vehicles.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: VehicleCell.reuseIdentifier, for: indexPath) as! VehicleCell
        cell.configure(vehicles[indexPath.row])
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        presenter?.showQR(of: vehicles[indexPath.row])
        tableView.deselectRow(at: indexPath, animated: true)
    }
}
