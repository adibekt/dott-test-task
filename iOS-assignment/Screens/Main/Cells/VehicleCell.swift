//
//  VehicleCell.swift
//  iOS-assignment
//
//  Created by tair on 11.04.22.
//

import UIKit

class VehicleCell: UITableViewCell, ReusableView, NibProvidable {

    @IBOutlet weak var imgScooter: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func configure(_ data: Vehicle) {
        if #available(iOS 15.0, *) {
            imgScooter.image = data.color.getImage()?.preparingForDisplay()
        } else {
            imgScooter.image = data.color.getImage()
        }
        self.lblTitle.text = data.identificationCode
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
extension UIImage {
    static let scooter = UIImage(named: "scooter/blueYellow")!
}
