//
//  VehiclesModel.swift
//  iOS-assignment
//
//  Created by tair on 11.04.22.
//

import Foundation
import UIKit

enum Color: String, Decodable, CaseIterable {
    case blueYellow = "YellowBlue"
    case redGreen = "RedGreen"
    case blueRed = "BlueRed"
    case pinkYellow = "PinkYellow"
    
    func getImage() -> UIImage? {
        switch self {
        case .blueYellow:
            return UIImage(named: "scooter/blueYellow")
        case .redGreen:
            return UIImage(named: "scooter/redGreen")
        case .blueRed:
            return UIImage(named: "scooter/blueRed")
        case .pinkYellow:
            return UIImage(named: "scooter/pinkYellow")
        }
    }
    
    
}
struct VehiclesResponse: Decodable {
    var vehicles: [Vehicle]
}
struct Vehicle: Decodable {
    var color: Color
    var qrURL: URL
    var identificationCode: String
}
