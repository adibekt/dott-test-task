//
//  VehiclesPresenter.swift
//  iOS-assignment
//
//  Created by tair on 11.04.22.
//

import Foundation
import AssignmentUtility
import RxRelay
import RxSwift

protocol VehilclesPresenterProtocol {
    var view: VehilclesViewProtocol? { get }
    var vehiclesList: PublishSubject<[Vehicle]> { get }
    var isBusy: PublishSubject<Bool> { get }
    var errorText: PublishSubject<String> { get }
    func getData()
    func filter(by colors: [Color]) -> [Vehicle]
    func clearFilters()
    func openFilter()
    func showQR(of item: Vehicle)
}
protocol VehilclesViewProtocol: AnyObject, Filterred {
    // extendable
}

class VehilclesPresenter: VehilclesPresenterProtocol {
    
    weak var view: VehilclesViewProtocol?
    var coordinator: Coordinator?
    var allVehicles = [Vehicle]()
    var vehiclesList: PublishSubject<[Vehicle]> = PublishSubject()
    var isBusy: PublishSubject<Bool> = PublishSubject()
    var errorText: PublishSubject<String> = PublishSubject()
    var selectedColors: [Color] = Color.allCases
    
    init(view: VehilclesViewProtocol) {
        self.view = view
    }
    
    func getData() {
        isBusy.onNext(true)
        var disposable: Disposable?
        disposable = AssignmentUtility.requestVehiclesData().subscribe { data in
            self.isBusy.onNext(false)
            do {
                let response = try JSONDecoder.init().decode(VehiclesResponse.self, from: data)
                self.vehiclesList.onNext(response.vehicles)
                self.allVehicles = response.vehicles
                disposable?.dispose()
            } catch {
                self.errorText.onNext("Decoding error occured")
            }
        } onError: { error in
            disposable?.dispose()
            self.isBusy.onNext(false)
            self.errorText.onNext("Network error occured")
            print(error.localizedDescription)
        } onCompleted: {
            print("Completed")
        }
    }
    
    func filter(by colors: [Color]) -> [Vehicle] {
        return allVehicles.filter({ colors.contains($0.color) })
    }
    
    func clearFilters() {
        vehiclesList.onNext(allVehicles)
    }
    
    func openFilter() {
        coordinator?.showFilters(delegate: view)
    }
    
    func showQR(of item: Vehicle) {
        coordinator?.showQRCodeVC(of: item)
    }
    
}

// use this class only in unit tests !!
final class TestVehiclePresenter: VehilclesPresenter {
    /// Test data injection
    func setUpData(_ data: [Vehicle]) {
        self.allVehicles = data
    }
}
