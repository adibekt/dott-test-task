//
//  ErrorView.swift
//  iOS-assignment
//
//  Created by tair on 11.04.22.
//

import Foundation
import UIKit

class ErrorView: UIView {
    
    var buttonPressed: (() -> ())?
    
    lazy var lblInfo: UILabel = {
        let lbl = BodyLabel()
        lbl.textColor = .black
        lbl.textAlignment = .center
        lbl.text = "Some error occured\nMay be try once more?"
        return lbl
    }()
    lazy var btnRetry: UIButton = {
        let btn = UIButton()
        btn.setTitle("Retry", for: .normal)
        btn.setTitleColor(DottColorAsset.constructiveHighlightedButtonBackground.color, for: .normal)
        btn.titleLabel?.font = DottFont.header1
        return btn
    }()
    
    // MARK: - Lifecycle
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    func commonInit() {
        backgroundColor = .gray.withAlphaComponent(0.2)
        layer.cornerRadius = 20
        layer.masksToBounds = false
        lblInfo.frame = CGRect(x: 30, y: 50, width: frame.width - 60, height: 30)
        btnRetry.frame = CGRect(x: 50, y: 88, width: frame.width - 100, height: 44)
        addSubview(lblInfo)
        addSubview(btnRetry)

        btnRetry.addTarget(self, action: #selector(onButtonPressed), for: .touchUpInside)
    }
    
    
    @objc private func onButtonPressed() {
        buttonPressed?()
    }
}
