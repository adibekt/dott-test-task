//
//  Nibbed.swift
//  iOS-assignment
//
//  Created by tair on 11.04.22.
//

import Foundation
import UIKit

protocol Nibbed {
    static func instantiate() -> Self
}
extension Nibbed where Self: UIViewController {
    static func instantiate() -> Self {
        let name = String(describing: self)
        return self.init(nibName: name, bundle: nil)
    }
}
