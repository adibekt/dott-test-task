//
//  UIViewController+Extension.swift
//  iOS-assignment
//
//  Created by tair on 11.04.22.
//

import UIKit

extension UIViewController {
    
    var activityIndicatorTag: Int { return 999999 }
    
    func show(message: String) {
        let toastLabel = UILabel(frame: CGRect(x:  30, y: self.view.frame.origin.y + 80, width: self.view.frame.size.width - 60, height: 50))
        toastLabel.backgroundColor = UIColor.red.withAlphaComponent(0.7)
        toastLabel.textColor = UIColor.white
        toastLabel.textAlignment = .center
        toastLabel.text = message
        toastLabel.alpha = 1.0
        toastLabel.numberOfLines = 0
        toastLabel.layer.cornerRadius = 10;
        toastLabel.font = .systemFont(ofSize: 16, weight: .bold)
        toastLabel.clipsToBounds  =  true
        self.view.addSubview(toastLabel)
        UIView.animate(withDuration: 3.0, delay: 0.4, options: .curveEaseOut, animations: {
            toastLabel.alpha = 0.0
        }, completion: {(isCompleted) in
            toastLabel.removeFromSuperview()
        })
    }
    
    func startActivityIndicator() {
        DispatchQueue.main.async {
            var style: UIActivityIndicatorView.Style
            if #available(iOS 13.0, *) {
                style = .medium
            } else {
                style = .gray
            }
            let activityIndicator = UIActivityIndicatorView(style: style)
            
            activityIndicator.tag = self.activityIndicatorTag
            activityIndicator.center = self.view.center
            activityIndicator.hidesWhenStopped = true
            activityIndicator.startAnimating()
            self.view.addSubview(activityIndicator)
        }
    }
    
    func stopActivityIndicator() {
        DispatchQueue.main.async {
            if let activityIndicator = self.view.subviews.filter(
                { $0.tag == self.activityIndicatorTag}).first as? UIActivityIndicatorView {
                activityIndicator.stopAnimating()
                activityIndicator.removeFromSuperview()
            }
        }
    }
}
