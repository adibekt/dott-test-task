//
//  ReusableView.swift
//  iOS-assignment
//
//  Created by tair on 11.04.22.
//

import UIKit

protocol ReusableView {
    static var reuseIdentifier: String { get }
}

extension ReusableView {
    static var reuseIdentifier: String { "\(self)" }
}

protocol NibProvidable {
    static var nibName: String { get }
    static var nib: UINib { get }
}

extension NibProvidable {
    static var nibName: String {
        return "\(self)"
    }
    
    static var nib: UINib {
        return UINib(nibName: self.nibName, bundle: nil)
    }
    
    static var viewLoaded: UIView {
        return nib.instantiate(withOwner: nil, options: nil)[0] as! UIView
    }
}
