//
//  Coordinator.swift
//  iOS-assignment
//
//  Created by tair on 11.04.22.
//

import UIKit

final class Coordinator {
    
    var navigation: UINavigationController?
    
    func start() {
        navigation = UINavigationController()
        let vc = VehiclesViewController.instantiate()
        let presenter = VehilclesPresenter(view: vc)
        presenter.coordinator = self
        vc.presenter = presenter
        App.window?.rootViewController = navigation
        App.window?.makeKeyAndVisible()
        navigation?.setViewControllers([vc], animated: false)
        
    }

    func showFilters(delegate: Filterred?) {
        let vc = FiltersViewController.instantiate()
        vc.delegate = delegate
        vc.modalPresentationStyle = .popover
        navigation?.present(vc, animated: true, completion: nil)
    }
    
    func showQRCodeVC(of item: Vehicle) {
        let vc = VehicleViewController.instantiate()
        vc.item = item
        vc.modalPresentationStyle = .popover
        navigation?.present(vc, animated: true, completion: nil)
    }
}
