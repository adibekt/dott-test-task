//
//  iOS_assignmentTests.swift
//  iOS-assignmentTests
//

import XCTest
@testable import iOS_assignment

class iOS_assignmentTests: XCTestCase {

    func testFilter() {
        let view = MockVehicleView()
        let presenter = TestVehiclePresenter(view: view)
        presenter.setUpData(generateMockData())
        var result = presenter.filter(by: [.blueRed])
        XCTAssertTrue(result.count == 1)
        result = presenter.filter(by: [.blueRed, .pinkYellow])
        XCTAssertTrue(result.count == 4)
        result = presenter.filter(by: [.blueYellow])
        XCTAssertTrue(result.count == 0)
        result = presenter.filter(by: [.blueYellow, .redGreen, .blueRed])
        XCTAssertFalse(result.filter({ $0.color == .pinkYellow }).count > 0)
    }

    func generateMockData() -> [Vehicle] {
        let v = Vehicle(color: .blueRed, qrURL: URL(string: "https://google.com")!, identificationCode: "")
        let v1 = Vehicle(color: .redGreen, qrURL: URL(string: "https://google.com")!, identificationCode: "")
        let v2 = Vehicle(color: .redGreen, qrURL: URL(string: "https://google.com")!, identificationCode: "")
        let v3 = Vehicle(color: .pinkYellow, qrURL: URL(string: "https://google.com")!, identificationCode: "")
        let v4 = Vehicle(color: .pinkYellow, qrURL: URL(string: "https://google.com")!, identificationCode: "")
        let v5 = Vehicle(color: .pinkYellow, qrURL: URL(string: "https://google.com")!, identificationCode: "")
        return [v, v1, v2, v3, v4, v5]
    }
}
